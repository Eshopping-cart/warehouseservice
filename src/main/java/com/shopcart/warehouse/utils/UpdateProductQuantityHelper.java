package com.shopcart.warehouse.utils;

import com.shopcart.warehouse.repository.model.ProductQuantityDocument;
import org.springframework.stereotype.Component;

@Component
public class UpdateProductQuantityHelper {

	public ProductQuantityDocument updateProductQuantity(long availableQuantity, ProductQuantityDocument productQuantityDocument) {
		String quantity = productQuantityDocument.getQuantity();
		long new_quantity = availableQuantity - Long.parseLong(quantity);
		if(new_quantity < 0)
			throw new RuntimeException("Requested Quantity is more than currently available quantity");
		productQuantityDocument.setQuantity(Long.toString(new_quantity));
		return productQuantityDocument;
	}
}
