package com.shopcart.warehouse.utils;

import com.shopcart.warehouse.models.ProductQuantityDTO;
import com.shopcart.warehouse.models.ProductQuantityResponseDTO;
import com.shopcart.warehouse.repository.model.ProductQuantityDocument;
import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import org.springframework.stereotype.Component;

@Component
public class Translators {

    private MapperFactory mapperFactory;

    private MapperFacade mapperFacade;

    public Translators() {
        this.mapperFactory = new DefaultMapperFactory.Builder().build();
        mapperFactory.classMap(ProductQuantityDTO.class, ProductQuantityDocument.class).byDefault().register();
        mapperFactory.classMap(ProductQuantityDocument.class, ProductQuantityResponseDTO.class).byDefault().register();
        mapperFacade = mapperFactory.getMapperFacade();
    }

    public ProductQuantityDocument translateToProductQuantityDocument (ProductQuantityDTO productQuantityDTO) {
        ProductQuantityDocument productQuantityDocument = mapperFacade.map(productQuantityDTO, ProductQuantityDocument.class);
        return productQuantityDocument;
    }

    public ProductQuantityResponseDTO translateToProductQuantityResponseDTO(ProductQuantityDocument productQuantityDocument) {
        ProductQuantityResponseDTO productQuantityResponseDTO = mapperFacade.map(productQuantityDocument, ProductQuantityResponseDTO.class);
        return productQuantityResponseDTO;
    }

}
