package com.shopcart.warehouse.service;

import com.shopcart.warehouse.utils.Translators;
import com.shopcart.warehouse.utils.UpdateProductQuantityHelper;
import com.shopcart.warehouse.models.ProductQuantityDTO;
import com.shopcart.warehouse.models.ProductQuantityResponseDTO;
import com.shopcart.warehouse.repository.ProductQuantityRepository;
import com.shopcart.warehouse.repository.model.ProductQuantityDocument;
import common.shopcart.library.exceptions.BadRequestException;
import common.shopcart.library.exceptions.RecordNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class WarehouseService {

    @Autowired
    ProductQuantityRepository repository;
    @Autowired
    UpdateProductQuantityHelper updateProductQuantityHelper;
    @Autowired
    Translators translators;

    public ProductQuantityResponseDTO getProductQuantity(String id) {
        ProductQuantityDocument productQuantityDocument = repository.findById(id)
                .orElseThrow(() -> new RecordNotFoundException("Cannot find product with productId = "+id));
        return translators.translateToProductQuantityResponseDTO(productQuantityDocument);
    }

    public ProductQuantityResponseDTO updateQuantity(ProductQuantityDTO productQuantityDTO) {
        String productId = productQuantityDTO.getProductId();
        ProductQuantityDocument currentDocument = repository.findById(productId)
                .orElseThrow(() -> new BadRequestException("Cannot find product with productId = "+productId));
        ProductQuantityDocument incomingDocument = translators.translateToProductQuantityDocument(productQuantityDTO);
        long availableQuantity = Long.parseLong(currentDocument.getQuantity());
        ProductQuantityDocument newProductQuantityDocument = updateProductQuantityHelper.updateProductQuantity(availableQuantity, incomingDocument);
        newProductQuantityDocument = repository.save(newProductQuantityDocument);
        return translators.translateToProductQuantityResponseDTO(newProductQuantityDocument);
    }

    public ProductQuantityResponseDTO addNewProduct(ProductQuantityDTO productQuantityDTO) {
        ProductQuantityDocument productQuantityDocument = translators.translateToProductQuantityDocument(productQuantityDTO);
        productQuantityDocument =  repository.save(productQuantityDocument);
        return translators.translateToProductQuantityResponseDTO(productQuantityDocument);
    }
}
