package com.shopcart.warehouse.resource;

import com.shopcart.warehouse.models.ProductQuantityDTO;
import com.shopcart.warehouse.models.ProductQuantityResponseDTO;
import com.shopcart.warehouse.repository.model.ProductQuantityDocument;
import com.shopcart.warehouse.service.WarehouseService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/products/quantity")
public class ProductQuantityResource {

	@Autowired
	WarehouseService warehouseService;
	
	@ApiOperation(value = "Get product quantity" , tags = "Ware House Service" , response = ProductQuantityResponseDTO.class,
			produces = MediaType.APPLICATION_JSON_VALUE , consumes = MediaType.APPLICATION_JSON_VALUE)
	@GetMapping("/{id}")
	public ResponseEntity getProductQuantity(@PathVariable("id") String id) {
		ProductQuantityResponseDTO responseDTO = warehouseService.getProductQuantity(id);
		return ResponseEntity.ok()
				.body(responseDTO);
	}

	@ApiOperation(value = "Update product quantity ", response = ProductQuantityResponseDTO.class, tags = "Ware House Service",
			produces = MediaType.APPLICATION_JSON_VALUE , consumes = MediaType.APPLICATION_JSON_VALUE)
	@PutMapping("/{id}")
	public ResponseEntity updateQuantity(@RequestBody @Valid ProductQuantityDTO productQuantityDTO) {
		ProductQuantityResponseDTO responseDTO = warehouseService.updateQuantity(productQuantityDTO);
		return ResponseEntity.ok()
				.body(responseDTO);
	}

	@ApiOperation(value = "Add new Product", response = ProductQuantityDocument.class, tags = "Ware House Service",
			produces = MediaType.APPLICATION_JSON_VALUE , consumes = MediaType.APPLICATION_JSON_VALUE)
	@PostMapping("/newProduct")
	public ResponseEntity addNewProduct(@RequestBody  @Valid ProductQuantityDTO productQuantityDTO) {
		ProductQuantityResponseDTO responseDTO = warehouseService.addNewProduct(productQuantityDTO);
		return ResponseEntity.ok()
				.body(responseDTO);
	}
}
