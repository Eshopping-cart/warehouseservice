package com.shopcart.warehouse.repository.model;

import common.shopcart.library.constraints.QuantityConstraint;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "warehouseInfo")
public class ProductQuantityDocument {
	@Id
	private String productId;

	@QuantityConstraint
	private String quantity;

}
