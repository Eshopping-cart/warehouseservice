package com.shopcart.warehouse.models;

import common.shopcart.library.constraints.QuantityConstraint;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductQuantityDTO {
    private String productId;

    @QuantityConstraint
    private String quantity;
}
